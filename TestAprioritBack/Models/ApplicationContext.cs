﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TestAprioritBack.Models.Data;

namespace TestAprioritBack.Models
{
    public class ApplicationContext : DbContext
    {

        public DbSet<Position> Positions { get; set; }
        public DbSet<Person> Persons { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options): base(options)
        {
            Database.EnsureCreated();
        }

    }
}
