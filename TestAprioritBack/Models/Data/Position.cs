﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAprioritBack.Models.Data
{
    public class Position
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }
}
