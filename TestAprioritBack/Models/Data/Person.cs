﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestAprioritBack.Models.Data
{
    public class Person
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public DateTime HiringDate { get; set; }

        public DateTime? FireDate { get; set; } 

        public decimal Salary { get; set; }


        public int PositionId { get; set; }
        public Position Position { get; set; }


    }
}
