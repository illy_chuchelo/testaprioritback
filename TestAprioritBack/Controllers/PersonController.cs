﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TestAprioritBack.Models;
using TestAprioritBack.Models.Data;

namespace TestAprioritBack.Controllers
{
    [ApiController]
    [Route("api/persons")]
    public class PersonController : Controller
    {
        ApplicationContext db;
        public PersonController(ApplicationContext context)
        {
            db = context;
            if (!db.Persons.Any())
            {
                db.Persons.Add(new Person { Name = "Иван", Surname = "Маслов", Salary = 1500, PositionId = 1 });
                db.Persons.Add(new Person { Name = "Александр", Surname = "Прыгунок", Salary = 600, PositionId = 2 });
                db.Persons.Add(new Person { Name = "Анастасия", Surname = "Капустина", Salary = 1800, PositionId = 3 });
                db.SaveChanges();
            }
        }

        [HttpGet]
        public IEnumerable<Person> Get()
        {
            return db.Persons
                .Include(p => p.Position)
                .ToList();
        }

        [HttpPost]
        public IActionResult Post(Person person)
        {
            if (ModelState.IsValid)
            {
                db.Persons.Add(person);
                db.SaveChanges();
                return Ok(person);
            }
            return BadRequest(ModelState);
        }
    }
}
