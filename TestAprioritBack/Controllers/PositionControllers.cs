﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using TestAprioritBack.Models;
using TestAprioritBack.Models.Data;

namespace TestAprioritBack.Controllers
{

    [ApiController]
    [Route("api/positions")]
    public class PositionControllers : Controller
    {
        ApplicationContext db;
        public PositionControllers(ApplicationContext context)
        {
            db = context;
            if (!db.Positions.Any())
            {
                db.Positions.Add(new Position { Name = "Middle Dev" });
                db.Positions.Add(new Position { Name = "Junior QA" });
                db.Positions.Add(new Position { Name = "Senior BA" });
                db.SaveChanges();
            }
        }

        [HttpGet]
        public IEnumerable<Position> Get()
        {
            return db.Positions.ToList();
        }

        [HttpPost]
        public IActionResult Post(Position position)
        {
            if (ModelState.IsValid)
            {
                db.Positions.Add(position);
                db.SaveChanges();
                return Ok(position);
            }
            return BadRequest(ModelState);
        }

    }
}
